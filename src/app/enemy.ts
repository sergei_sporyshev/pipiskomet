import * as PIXI from 'pixi.js';
import Sprite = PIXI.Sprite;

export class Enemy extends Sprite{

    speed: number;

    constructor(texture : any, width: number){
        super(texture);
        this.y = -1 * this.height;
        this.x = Math.random() * (width - 200 - 200) + 200;
        this.anchor.x = 0.5;
        this.alpha = 1;
        this.speed = Math.floor(Math.random() * (5-2) + 2);

    }

    move(){
        this.y += this.speed;
    }
}

