import * as PIXI from 'pixi.js';
import Sprite = PIXI.Sprite;

export class Player extends Sprite{

    speed :number = 7;
    directionX :number = 0;
    xLim :number;

    constructor(texture : any, startX :number, startY :number, areaSize :any){
        super(texture);
        this.y = startY;
        this.x = startX;
        this.xLim = areaSize.width - texture.width;
    }

    move(direction:any)  {
        if(direction[37]){
            this.directionX = -1
        }else if(direction[39]){
            this.directionX = 1;
        }else{
            this.directionX = 0;
        }
        let nextX = this.x + this.directionX * this.speed;
        if(nextX < 5 && this.directionX < 0)
            return;

        if(nextX > this.xLim - 5 && this.directionX > 0)
            return;

        this.x = nextX;
    }

    shoot(){
        return this.x + 24;
    }
}

