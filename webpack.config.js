const path = require('path');
const webpack = require('webpack');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new CopyPlugin([
            { from: 'src/assets', to: 'assets' },
        ])
    ],
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, 'dist'),
        sourceMapFilename: "[name].js.map"
    },
    module: {
        rules: [
            { test: /\.ts$/, use: 'ts-loader' },
            {
                test: /\.s?css$/,
                use: [
                    // style-loader
                    { loader: 'style-loader'},
                    // css-loader
                    { loader: 'css-loader'/*, options: {modules: true}*/},
                    // sass-loader
                    { loader: 'sass-loader' }
                ],
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        symlinks: true,
        modules: ['src', 'node_modules'],
        extensions: [ '.tsx', '.ts', '.js' ],
    },
    devtool: "source-map"
};
